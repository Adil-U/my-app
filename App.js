import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  const [state, setState] = React.useState([])
  
  React.useEffect(() => {
    try {
      fetch('http://95.216.210.131:3000/chats')
      .then(response => response.json())
      .then(data => setState(data))
    } catch (error) {
      console.log('error', error)
    }
  },[])

  console.log('state', state)

  return (
    <View style={styles.container}>
      {state.map((item, index) => (
        <Text key={item?._id}>{item?.title}</Text>
      ))}
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
